import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        int numeroAtual,maior,menor;
        boolean primeiro = true;
        Imprime tela = new Imprime();

        tela.imprime("Digite um número (negativo para sair): ");
        numeroAtual = entrada.nextInt();

        if(numeroAtual>=0){
            menor = numeroAtual;
            maior = numeroAtual;
            while(numeroAtual>=0){
                tela.imprime("Digite outro número: ");
                numeroAtual = entrada.nextInt();
                if(numeroAtual > maior){
                    maior = numeroAtual;
                }
                if(numeroAtual < menor && numeroAtual >= 0){
                    menor = numeroAtual;
                }
            }
            tela.imprime("\n O maior número foi "+maior+" e o menor foi "+menor);
        }


    }


}